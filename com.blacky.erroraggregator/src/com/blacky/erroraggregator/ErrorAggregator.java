package com.blacky.erroraggregator;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class ErrorAggregator {
	public FileWriter errorLog, errorSummary;
	public int lineNumber;
	static boolean failure = false;

	public void createOutputFiles(String path) throws IOException{
		errorLog = new FileWriter(path + "/errorLog.txt");
		errorSummary = new FileWriter(path + "/summary.txt");
	}
	
	public void ReadFile(String filename, boolean close) throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(filename));
		lineNumber = 0;
		processLine(filename, true);
		for (String line; (line = br.readLine()) != null;){
			processLine(line);
//			System.out.println(line);
		}
		if (close)
			errorLog.close();
		br.close();
	}
	
	public void PrintFile(String filename) throws IOException{
		BufferedReader print = new BufferedReader(new FileReader(filename));
		System.out.println("Showing contents of " + filename);
		for (String line; (line = print.readLine()) != null;){
			System.out.println(line);
		}
		print.close();
	}
	
	public void processLine (String filename, boolean title) throws IOException{
		if (title){
			errorLog.write(System.lineSeparator());
			errorLog.write("Error Log for " + filename);
			errorLog.write(System.lineSeparator());
		}
		else
			processLine(filename);
	}	
	
	public void processLine (String logLine) throws IOException{
		lineNumber++;
		if (logLine.contains("Error")){
			errorLog.write("Line " + lineNumber + ": ");
			errorLog.write(logLine);
			errorLog.write(System.lineSeparator());
		}
	}
	
	
	public void ReadFolder (File folder) throws IOException{
		File[] listOfFiles = folder.listFiles();
		if (!(folder.exists())){
			System.out.println("Log files directory (D:\\temp) missing!");
			System.exit(0);
		}
		if (listOfFiles.length == 0){
			System.out.println("No log files in Directory!");
			failure = true;
		}
		else{
			System.out.println("There are " + listOfFiles.length + " files in " + folder.toString());
			for (File file:listOfFiles){
				String[] fileprops = file.getName().split("\\.");
				if (fileprops[fileprops.length-1].equalsIgnoreCase("log")){
					ReadFile(file.getAbsolutePath(), false);
				}
			}
			errorLog.close();
		}
	}

	public void deleteLogFiles(String errorlog, String summary) throws IOException{
		Files.deleteIfExists(Paths.get(errorlog));
		Files.deleteIfExists(Paths.get(summary));
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		ErrorAggregator ea = new ErrorAggregator();
//		String foldername = "D:\\temp";
		String foldername = new File("").getAbsolutePath() + "/com.blacky.erroraggregator/Logs";
		System.out.println(foldername);
		String errorLog = foldername + "/errorLog.txt";
		String summary = foldername + "/summary.txt";
		File folder = new File(foldername);
		File[] fileList;

		failure = false;
		ea.deleteLogFiles(errorLog,summary);
		ea.createOutputFiles(foldername);
		
		switch (args.length){
		case 0:
			ea.ReadFolder(folder);
			if (!failure)
			ea.PrintFile(foldername + "/errorLog.txt");
			break;
		case 1:
			switch (args[0]){
			case "summary":
				break;
			default:
				ea.ReadFile(foldername + "/" + args[0], true);
				ea.PrintFile(errorLog);
				
			}
			break;
		default:
			System.out.println("Some error Occurred. Only one argument supported!");
		}
		

	}

}
